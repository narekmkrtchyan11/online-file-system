import enTranslations from "../assets/translate/en.json";
import hyTranslations from "../assets/translate/hy.json";
import { Language } from "./enums";
import { ObjectDTO } from "./interfaces";

const translations: ObjectDTO = {
    [Language.en]: enTranslations,
    [Language.hy]: hyTranslations,
}

export  function translate(key: string, valuesObj: any = {},  defaultValue = key): string {
    const lang: string = localStorage.getItem("lang") || Language.en;
    let result: ObjectDTO = translations[lang];
  
    if (key && result) {
        const paths = key.split('.');

        for (const path of paths) {
            result = result[path];

            if (result === undefined) {
                return defaultValue;
            }
        }

        if (typeof result === "string") {
            return Object.keys(valuesObj).reduce((text, key) => (
                text.replaceAll(`{${key}}`, valuesObj[key])
            ), result);
        }

        return defaultValue;
    }

    return defaultValue;
}