import { SystemField, Path } from './interfaces';

export function updateTreeNode(tree: SystemField, newNode: SystemField, paths: Path[]) {
    const [currentPath, ...restPaths] = paths;
    if (currentPath.id == tree.id) {
      if (!restPaths.length) {
        return newNode;
      }
   
     return {
       ...tree,
       data: tree.data.map((node: SystemField) => (
         updateTreeNode(node, newNode, restPaths)
       )),
     }
    }

    return tree;
  }

  function click (node: HTMLElement) {
    try {
      node.dispatchEvent(new MouseEvent('click'));
    } catch (e) {
      const evt = document.createEvent('MouseEvents');
      evt.initMouseEvent('click', true, true, window, 0, 0, 0, 80,
                            20, false, false, false, false, 0, null);
      node.dispatchEvent(evt);
    }
}
  
export function download(path: string, name: string) {
    const a: HTMLAnchorElement = document.createElementNS('http://www.w3.org/1999/xhtml', 'a') as HTMLAnchorElement;
    name = name || 'download';
    a.download = name;
    a.rel = 'noopener';
    a.href = path;
    setTimeout(function () { click(a) }, 0);
}

export function saveAs (blob: Blob, name: string) {
    const URL = window.URL || window.webkitURL;
    const path = URL.createObjectURL(blob);
    download(path, name);
    setTimeout(function () { URL.revokeObjectURL(path) }, 4E4);
}
