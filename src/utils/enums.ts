export enum FieldType {
    text = 'text',
    folder = 'folder',
    file = 'file',
    url = 'url'
};

export enum Language {
    en = 'en',
    hy = 'hy'
};
