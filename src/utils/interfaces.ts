import { FieldType } from './enums';

export type Callback<T = void, W extends any[] = any> = (...args: W) => T;

export type SystemField = {
    id: string;
    type: FieldType;
    title: string;
    data: any;
}

export type FolderField = SystemField & {
    data: SystemField[];
};

export type TextField = SystemField & {
    data: string;
};

export type CustomFileField = SystemField & {
    data: string;
    fileType: string;
};

export type Path = {
    id: string;
    title: string;
};

export type ObjectDTO = {
    [key: string]: any,
}