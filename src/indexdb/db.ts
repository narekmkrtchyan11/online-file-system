import Dexie, { Table } from 'dexie';
import { FolderField } from '../utils/interfaces';
 
export class MySubClassedDexie extends Dexie {
  tree!: Table<FolderField>;

  constructor() {
    super("myDrive");
    this.version(2).stores({
      tree: "id, type, title, data",
    });
  }
}

export const db = new MySubClassedDexie();
