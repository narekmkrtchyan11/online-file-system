import { FunctionComponent, useState } from "react";

import { CustomFileField, FolderField } from "../../utils/interfaces";
import { useRouteContext } from "../../SystemRoute";
import { FieldType } from '../../utils/enums';
import { translate } from "../../utils/translate";
import NewItemModal from "../new-item-modal/NewItemModal";
import SystemItems from "../system-items/SystemItems";
import AddButton from "../add-items/AddItem";
import Navbar from "../navbar/Navbar";
import Popup from "../popup/Popup";
import Header from "../header/Header";
import Languages from "../languages/Languages";

const MainPage:FunctionComponent = () => {
    const [showPopup, setShowPopup] = useState(false);
    const [showNewItemModal, setNewShowItemModal] = useState(false);
    const [fieldType, setFieldType] = useState<FieldType | null>(null);
    const [language, setLanguage] = useState<string>("");

    function handleSelectChange(e: React.ChangeEvent<HTMLSelectElement>) {
        const lang = e.target.value;
        setLanguage(lang)
        localStorage.setItem("lang", lang)
    }
    
    const { currentNode, updateTree } = useRouteContext();
    
    function clickHandler(){
        setShowPopup(true);
    }

    function addNewFolder() {
        setShowPopup(false);
        setNewShowItemModal(true);
        setFieldType(FieldType.folder);
    }
    
    function addFile(fileNode: CustomFileField) {
        updateTree({
            ...currentNode,
            data: [
                ...currentNode?.data,
                fileNode,
            ],
        } as FolderField);
        setShowPopup(false);
    }

    function addNewTextFile() {
        setShowPopup(false);
        setNewShowItemModal(true);
        setFieldType(FieldType.text);
    }

    function addUrl() {
        setShowPopup(false);
        setNewShowItemModal(true);
        setFieldType(FieldType.url);
    }

    function formHandler(e:MouseEvent, title: string, data: any, type: FieldType) {
        e.preventDefault();
        setNewShowItemModal(false);

        if(type === FieldType.folder){
            updateTree({
                ...currentNode,
                data: [
                    ...currentNode?.data,
                    {
                        id: `${new Date().getTime()}`,
                        type: FieldType.folder,
                        title,
                        data: [],
                    },
                ],
            } as FolderField);
        } else if(type === FieldType.text){
            updateTree({
                ...currentNode,
                data: [
                    ...currentNode?.data,
                    {
                        id: `${new Date().getTime()}`,
                        type: FieldType.text,
                        title,
                        data: data.trim().length === 0 ? translate("placeholders.empty") : data
                    },
                ],
            } as FolderField);
        } else if(type === FieldType.url) {
            updateTree({
                ...currentNode,
                data: [
                    ...currentNode?.data,
                    {
                        id: `${new Date().getTime()}`,
                        type : FieldType.url,
                        title,
                        data : data
                    }
                ]
            } as FolderField)
        }
    }

    return(
        <>
           <Header>
                <Navbar/>
                <AddButton openPopupHandler={clickHandler}/> 
                <Languages  handleSelectChange={handleSelectChange}/>
            </Header>
            <SystemItems />
            {showPopup && (
                <Popup
                    addFile={addFile}
                    closePopup={() => setShowPopup(false)} 
                    addNewFolder={addNewFolder}
                    addNewTextFile={addNewTextFile}
                    addUrl={addUrl}
                />
            )}
            {showNewItemModal && (
                <NewItemModal
                    cancelCreateingNewItem={() => setNewShowItemModal(false)} 
                    formHandler={formHandler}
                    fieldType={fieldType}
                />
            )}
        </>
    );
}

export default MainPage