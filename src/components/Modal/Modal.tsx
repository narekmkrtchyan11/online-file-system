import { FunctionComponent, ReactNode } from 'react';

import { Callback } from '../../utils/interfaces';

import './Modal.css';

type Props = {
    title: ReactNode,
    children: ReactNode,
    onCancel: Callback,
    onAccept?: Callback
    acceptLabel?: string,
    cancelLabel?: string
}

const Modal: FunctionComponent<Props> = ({
    title,
    children,
    acceptLabel = "Accept",
    cancelLabel = "Cancel",
    onAccept,
    onCancel
}) => (
    <>
        <div className="my-modal" >
            <div className="modal-wrapper" >
                <div className="modal-title">
                    <h2 className='modal-title-text'>{title}</h2>
                </div>
                <div className="modal-content">
                    {children}
                </div>
                <div className="modal-buttons">
                    <button
                        onClick={onCancel}
                        className="cancelBtn"
                        type="button"
                    >
                        {cancelLabel}
                    </button>
                    {onAccept && <button
                        onClick={onAccept}
                        className="acceptBtn"
                        type="submit"
                    >
                        {acceptLabel}
                    </button>
                    }
                </div>
            </div>
        </div>
    </>
)
export default Modal;