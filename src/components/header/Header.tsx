import { FunctionComponent } from "react";

import './Header.css'

type Props = {
    children: React.ReactNode;
}

const Header: FunctionComponent<Props> = ({ children }) => {
    return (
        <div className="header">
            {children}
        </div>
    )
}

export default Header

