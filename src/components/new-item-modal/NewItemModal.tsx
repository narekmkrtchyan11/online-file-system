import { FormEvent, FunctionComponent, useRef } from "react";

import { Callback } from "../../utils/interfaces";
import { translate } from "../../utils/translate";
import { FieldType } from '../../utils/enums';
import Modal from "../Modal/Modal";

import "./NewItemModal.css";

type Props = {
    cancelCreateingNewItem: Callback,
    formHandler: Callback,
    fieldType: FieldType | null,
}

const NewItemModal: FunctionComponent<Props> = ({
    cancelCreateingNewItem,
    formHandler,
    fieldType,
}) => {
    const textAreaRef = useRef<HTMLTextAreaElement>(null);
    const urlInnputRef = useRef<HTMLInputElement>(null);
    const titleInput = useRef<HTMLInputElement>(null);

    function submitHandler(e: FormEvent) {
        formHandler(
            e,
            titleInput.current?.value || translate(`placeholders.untitled_${fieldType}`),
            fieldType === FieldType.url
                ? urlInnputRef.current?.value
                : textAreaRef.current?.value,
            fieldType,
        );
    }

    return (
        <Modal
            title={translate(`fieldTypes.${fieldType}`)}
            acceptLabel={translate("general.create")}
            onCancel={cancelCreateingNewItem}
            onAccept={submitHandler}
        >
            <input
                type="text" 
                className="titleInput" 
                placeholder={translate(`placeholders.untitled_${fieldType}`)}
                ref={titleInput}
                autoFocus
            />
            {(fieldType === FieldType.text) && 
                <textarea 
                    ref={textAreaRef} 
                    className="textArea" 
                    placeholder={translate("placeholders.text")}
                >
                    </textarea>
            }
            {(fieldType === FieldType.url) && 
                <input
                    ref={urlInnputRef}
                    className="input"
                    placeholder={translate("placeholders.url")}
                />
            }
        </Modal>
    )
}

export default NewItemModal;