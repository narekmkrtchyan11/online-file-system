import { FunctionComponent } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolder, faFileLines, faDownload, faLink } from '@fortawesome/free-solid-svg-icons';

import { Callback } from '../../utils/interfaces';
import { FieldType } from '../../utils/enums';
import { translate } from '../../utils/translate';

import "./Popup.css";

type Props = {
    closePopup: Callback,
    addNewFolder: Callback,
    addNewTextFile: Callback,
    addFile: Callback,
    addUrl: Callback
}

const Popup: FunctionComponent<Props> = ({
    closePopup,
    addNewFolder,
    addNewTextFile,
    addFile,
    addUrl
}) => {
    function addCustomFile (e:any) {
        const file = e.target.files[0];
        const reader = new FileReader();
        let data:string = "";

        reader.addEventListener("load", () => {
            data = reader.result as string;
            addFile({
                id: `${new Date().getTime()}`,
                type: FieldType.file,
                title: file.name,
                fileType: file.type,
                data,
            });
        }, false);

        reader.readAsDataURL(file);
    }

    return (
        <>
            <div className="modal" onClick={closePopup} ></div>
            <div className="popup">
                <ul className='popup-list'>
                    <li onClick={addNewFolder}>
                        <FontAwesomeIcon icon={faFolder} className="icon"/>
                        <span>{translate("fieldTypes.folder")}</span>
                    </li>
                    <li onClick={addNewTextFile}>
                        <FontAwesomeIcon icon={faFileLines} className="icon"/>
                        <span>{translate("fieldTypes.text")}</span>
                    </li>
                    <li >
                        <label htmlFor="upload">
                            <FontAwesomeIcon icon={faDownload} className="icon"/>
                            <span>{translate("fieldTypes.uploadFile")}</span>
                        </label>
                        <input 
                            onChange={addCustomFile} 
                            style={{width: 0, height: 0}} 
                            type="file" 
                            id="upload"
                        />
                    </li>
                    <li onClick={addUrl}>
                        <FontAwesomeIcon icon={faLink} className="icon"/>
                        <span>{translate("fieldTypes.url")}</span>
                    </li>
                </ul>
            </div>
        </>
    );
}

export default Popup;