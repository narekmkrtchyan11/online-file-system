import { FunctionComponent } from "react";
import { useRouteContext } from "../../SystemRoute";

import "./Navbar.css";

const Navbar: FunctionComponent = () => {
    const { currentPath, changePath } = useRouteContext();

    function handleNavigate(index: number) {
        changePath(currentPath.slice(0, index + 1));
    }

    return (
        <div className="navigation">
            <div>
                <ul className="pathes">
                    {currentPath.map(({ title, id }, i) => (
                        <li className="path" key={id} onClick={() => handleNavigate(i)}>
                            <b className="linkItem">{title || 'root'}</b>
                            <span className="delimiter">/</span>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
};

export default Navbar;