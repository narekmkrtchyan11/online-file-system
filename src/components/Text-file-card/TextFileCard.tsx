import { FunctionComponent, useState } from "react";

import TextFilePopup from "../text-file-popup/TextFilePopup";
import { Callback } from "../../utils/interfaces";
import { saveAs } from "../../utils/helpers";
import CardItem from "../card-item/CardItem";

import textFileSvg from '../../svg/doc.svg';

import styles from "./textFileCard.module.css"

type Props = {
    title: string,
    text: string,
    removeItem: Callback,
    onDelete: Callback
};

const TextFileCard: FunctionComponent<Props> = ({
    title,
    text,
    onDelete,
}) => {
    const [showTextFilePopup, setShowTextFilePopup] = useState(false);

    function openTextFilePopup() {
        setShowTextFilePopup(true);
    }

    function handleDownload() {
        const blob = new Blob([text],{ type: "text/plain;charset=utf-8" });
        saveAs(blob, title)
    }

    return (
        <CardItem
            title={title}
            onDelete={onDelete}
            withDownload={true}
            onDownload={handleDownload}
        >
        {showTextFilePopup && (
            <TextFilePopup 
                closeTextFilePopupHandler={() => setShowTextFilePopup(false)}
                title={title}
                text={text}
            />
        )}
            <div
                onDoubleClick={openTextFilePopup}
                className={styles.textFile}
            >
                <img src={textFileSvg} className={styles.textFileIcon}/>
            </div>
        </CardItem>
    );
}

export default TextFileCard;