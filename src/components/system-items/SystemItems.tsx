import { FunctionComponent, useState } from "react";

import { CustomFileField, FolderField, TextField, SystemField } from '../../utils/interfaces';
import { useRouteContext } from "../../SystemRoute";
import { FieldType } from "../../utils/enums";
import { translate }from "../../utils/translate";
import LinkItemCard from "../link-item-card/LinkItemCard";
import FolderCard from "../FolderCard/FolderCard";
import TextFileCard from "../Text-file-card/TextFileCard";
import CustomFileCard from "../custom-file-card/CustomFileCard";
import DeleteItemPopup from "../delete-item-popup/DeleteItemPopup";

import "./SystemItems.css";

const SystemItems: FunctionComponent = () => {
    const { currentNode, updateTree } = useRouteContext();
    const [deletePopupParams, setDeletePopupParams] = useState<{
         id: string;
         type: FieldType;
        } | null>(null);

    function removeItem(id: string){
        updateTree({
            ...currentNode,
            data: currentNode?.data.filter((item: SystemField) => {
                return item.id !== id
            }),
        } as FolderField);
    }
    
    function openDelete(id: string, type: FieldType) {
        setDeletePopupParams({
            id,
            type,
        });
    }

    function closeDelete() {
        setDeletePopupParams(null);
    }

    function handleReove() {
        if (deletePopupParams) {
            removeItem(deletePopupParams.id);
        }
        closeDelete();
    }

    return(
        <div className="items">
            {!currentNode?.data.length && translate("placeholders.noItems")}
            {currentNode?.data.map((item: SystemField) => {
                if(item.type === FieldType.folder){
                    return(
                        <FolderCard
                            onDelete={() => openDelete(item.id, item.type)}
                            title={item.title}
                            key={item.id}
                            id={item.id}
                        />
                    )
                } else if(item.type === FieldType.text) {
                    return(
                        <TextFileCard 
                            title={item.title.trim().length !== 0 ? item.title : translate("placeholders.untitledTextFile")} 
                            removeItem={() => removeItem(item.id)}
                            onDelete={() => openDelete(item.id, item.type)}
                            text={(item as TextField).data}
                            key={item.id}
                        />
                    )
                } else if (item.type === FieldType.url) {
                    return (
                        <LinkItemCard
                            onDelete={() => openDelete(item.id, item.type)}
                            title={item.title}
                            data={item.data}
                            key={item.id}
                        />
                    );
                } else {
                    return(
                        <CustomFileCard 
                            fileType={(item as CustomFileField).fileType}
                            data={(item as CustomFileField).data}
                            title={item.title}
                            key={item.id}
                            onDelete={() => openDelete(item.id, item.type)}
                        />
                    );
                }
            })}
            {!!deletePopupParams && (
                <DeleteItemPopup
                    onCancel={closeDelete} 
                    onAccept={handleReove}
                    type={deletePopupParams.type}
                />
            )}
        </div>
    )
}

export default SystemItems;