import { FunctionComponent, useState } from "react";

import ImagePopup from "../image-popup/ImagePopup";
import { Callback } from "../../utils/interfaces";
import { download } from "../../utils/helpers";
import CardItem from "../card-item/CardItem";

import fileSvg from '../../svg/file.svg';

import  styles  from  "./customFileCard.module.css";

type Props = {
    title: string,
    onDelete: Callback,
    data: string,
    fileType: string
}

const CustomFileCard: FunctionComponent<Props> = ({ title, onDelete, data, fileType }) => {
    const [showImagePopup, setShowImagePopup] = useState(false);
    const isImage = fileType.split('/')[0] === 'image';

    return (
        <CardItem
            title={title}
            onDelete={onDelete}
            withDownload={isImage}
            onDownload={() => download(data, title)}
        >
            {showImagePopup && <ImagePopup imgSrc={data} setShowImagePopup={setShowImagePopup} />}
            <div
                className={styles.customFile}
                onClick={() => {
                    if(isImage) {
                        setShowImagePopup(true)
                    } else {
                        download(data, title)
                    }
                }}
            >
                {isImage ? 
                    <>
                        <img src={data} className={styles.customFileImg}/>
                    </> : 
                    <img src={fileSvg} className={styles.customFileIcon}/>
                }
            </div>
        </CardItem>
    )
}

export default CustomFileCard;