import { FunctionComponent, PropsWithChildren } from "react";

import { Callback } from "../../utils/interfaces";

import downloadSvg from '../../svg/download.svg';
import deleteSvg from '../../svg/delete.svg';

import styles from "./cardItem.module.css";

type Props = PropsWithChildren<{
    title: string,
    onDelete: Callback,
    onDownload?: Callback,
    withDownload?: boolean,
}>;

const CardItem: FunctionComponent<Props> = ({
     title,
     children,
     onDelete,
     onDownload,
     withDownload,
}) => {

    return (
        <div className={styles.card}>
            <div className={styles.content}>
                {children}
                <div className={styles.actions}>
                    <img 
                        src={deleteSvg} 
                        className={styles.icon}
                        onClick={onDelete}
                    />
                    {withDownload && (
                        <img 
                            className={styles.icon}
                            onClick={onDownload}
                            src={downloadSvg}
                        />
                    )}
                </div>
            </div>
            <div className={styles.footer}>
                <p className={styles.title}>{title}</p>
            </div>
        </div>
    )
}

export default CardItem;