import { FunctionComponent } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import { Callback } from "../../utils/interfaces";
import { translate } from "../../utils/translate";

import "./AddItem.css";

type Props = {
    openPopupHandler: Callback;
}

const AddButton: FunctionComponent<Props> = (props) => {
    return (
        <button className="addBtn" onClick={props.openPopupHandler}>
            <FontAwesomeIcon icon={faPlus} className="fa-solid fa-plus"/>
            {translate("general.new")}
        </button> 
    );
};

export default AddButton;