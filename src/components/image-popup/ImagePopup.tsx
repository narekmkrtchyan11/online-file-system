import { FunctionComponent } from "react";

import { Callback } from "../../utils/interfaces";

import "./ImagePopup.css"

type Props = {
    imgSrc: string,
    setShowImagePopup: Callback
}

const ImagePopup: FunctionComponent<Props> = ({ imgSrc, setShowImagePopup }) => {
    return(
        <>
            <div className="imgPopupModal" onClick={() => setShowImagePopup(false)}></div>
            <div className="imagePopup">
                <img className="ImagePopupImg" src={imgSrc}></img>
            </div>
        </>
    );
}

export default ImagePopup;