import { FunctionComponent } from "react";

import { Callback } from "../../utils/interfaces";
import { translate } from "../../utils/translate";
import Modal from '../Modal/Modal';

type Props = {
    text: string,
    title: string,
    closeTextFilePopupHandler: Callback
}

const TextFilePopup: FunctionComponent<Props> = ({
    title,
    text,
    closeTextFilePopupHandler,
}) => {
    return(
        <Modal
            cancelLabel={translate("general.close")}
            onCancel={closeTextFilePopupHandler}
            title={title}
        > 
            <p className="textFilePopupText">
                 {text} 
            </p>
        </Modal>
    );
};

export default TextFilePopup;