import { FunctionComponent } from "react";

import { Callback } from "../../utils/interfaces";
import { translate } from "../../utils/translate";
import { Language } from "../../utils/enums";

import "./Languages.css"

type Props = {
    handleSelectChange: Callback,
}

const Languages: FunctionComponent<Props> = ({ handleSelectChange }) => {
    return(
        <div className="languagesContainer">
            <select     
                className="languagesSelect" 
                onChange={handleSelectChange}
                value={localStorage.getItem("lang") || Language.en}
            >
                <option value={Language.en} className="english">
                    🇺🇸{translate("languages.en")}
                </option>
                <option value={Language.hy} className="armenian">
                    🇦🇲{translate("languages.hy")}
                </option>
            </select>
        </div>
    );
}

export default Languages;