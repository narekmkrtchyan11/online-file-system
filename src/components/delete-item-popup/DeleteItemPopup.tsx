import { FunctionComponent } from "react";

import { Callback } from "../../utils/interfaces";
import { translate } from "../../utils/translate";
import { FieldType } from "../../utils/enums";
import Modal from "../Modal/Modal";

type Props = {
    setShowDeletePopup?: Callback,
    onCancel: Callback,
    onAccept?: Callback,
    removeItem?: Callback,
    type: FieldType
}

const DeleteItemPopup: FunctionComponent<Props> = ({
    onCancel,
    onAccept,
    type,
}) => (
    <Modal
        title={translate("popup.deleteItemPopup.title")}
        acceptLabel={translate("general.delete")}
        onCancel={onCancel}
        onAccept={onAccept}
    >
        <h2>{translate("popup.deleteItemPopup.text")} <b>{type}</b>?</h2>
    </Modal>
);

export default DeleteItemPopup;
