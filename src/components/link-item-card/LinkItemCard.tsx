import { FunctionComponent } from 'react';

import { Callback } from '../../utils/interfaces';
import CardItem from '../card-item/CardItem';

import urlSvg from '../../svg/url.svg';

import styles from './linkItemCard.module.css';

type Props = {
    title: string,
    data: string,
    onDelete: Callback,
}

const LinkItem: FunctionComponent<Props> = ({
    title,
    data,
    onDelete,
}) => {

    function redirect () {
        window?.open(data, '_blank')?.focus();
    }

    return (
        <CardItem
            title={title}
            onDelete={onDelete}
        >
            <div
                className={styles.link} 
                onDoubleClick={redirect}
            >
                <img src={urlSvg} className={styles.icon}></img>
            </div>
        </CardItem>
    )
}

export default LinkItem;