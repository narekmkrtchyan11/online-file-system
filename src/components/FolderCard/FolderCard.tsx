import { FunctionComponent } from 'react';

import { useRouteContext } from '../../SystemRoute';
import { Callback } from '../../utils/interfaces';
import CardItem from '../card-item/CardItem';

import folderSvg from '../../svg/Folder.svg';

import styles from './folderCard.module.css';

type Props = {
    title: string,
    id: string,
    onDelete: Callback,
}

const FolderCard: FunctionComponent<Props> = ({
    title,
    id,
    onDelete,
}) => {
    const { changePath, currentPath } = useRouteContext();

    function redirect () {
        changePath([...currentPath, { id, title }])
    }

    return (
        <CardItem
            title={title}
            onDelete={onDelete}
        >
            <img
                src={folderSvg}
                className={styles.folderIcon}
                onDoubleClick={redirect}
            />
        </CardItem>
    )
}

export default FolderCard;