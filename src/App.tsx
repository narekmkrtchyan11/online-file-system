import MainPage from './components/main/MainPage';
import { SystemRoute } from './SystemRoute';

function App() {
  return (
    <SystemRoute>
      <MainPage />
    </SystemRoute>
  )
}

export default App;
