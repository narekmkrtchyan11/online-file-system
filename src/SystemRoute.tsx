import { useEffect, useState, FunctionComponent, createContext, useContext } from "react";
import { useLiveQuery } from "dexie-react-hooks";

import { db } from "./indexdb/db";
import { FolderField, SystemField, Callback, Path } from './utils/interfaces';
import { FieldType } from "./utils/enums";
import { updateTreeNode } from './utils/helpers';

type RouteContextProps = {
  status: string;
  updateTree: Callback<void, [SystemField]>;
  currentNode: SystemField | null;
  currentPath: Path[];
  changePath: Callback<void, [Path[]]>;
};

const Context = createContext<RouteContextProps>({
  status: '',
  updateTree: () => null,
  currentNode: null,
  currentPath: [],
  changePath: () => null,
});

type Props = {
  children: any;
};

const SystemRoute: FunctionComponent<Props> =  ({ children }) => {
  const [currentPath, setCurrentPath] = useState(() =>
    (JSON.parse(localStorage.getItem('rout-path') || 'null') || [{ id: '/', title: '' }])
  );
  const [isLoaded, setIsLoaded] = useState(false);
  const [currentNode, setCurrentNode] = useState<SystemField | null>(null);
  const [treeObj, setTreeObj] = useState<FolderField>({
    id: '/',
    title: '',
    type: FieldType.folder,
    data: [],
  });
  const [status, setStatus] = useState("");

  const list =  useLiveQuery(() => db.tree.toArray());

  useEffect(() => {
    if (list && !isLoaded) {
      const treeData:FolderField = list?.[0] || treeObj;

      setTreeObj(treeData)
      setIsLoaded(true);
    }
  }, [list]);

  useEffect(() => {
    let node: FolderField | null = null;
    const [rootPath, ...paths] = currentPath;
 
    if (rootPath.id === treeObj.id) {
      node = treeObj;

      for (let path of paths) {
        let nodeField: SystemField = node as SystemField;
        if (nodeField.type !== FieldType.folder || !nodeField.data?.length) {
          node = null;
          break
        };
        
        node = (nodeField.data as SystemField[]).find(({ id }) => id === path.id) || null;

        if (!node) {
          break;
        }
      }
    }

    setCurrentNode(node);
  }, [treeObj, currentPath]);

  function changePath(path: Path[]) {
    if (path.length) {
      setCurrentPath(path);
      localStorage.setItem("path", JSON.stringify(path));
    }
  }

  async function updateTree(newCurrentTree: FolderField) {
    let newTree = updateTreeNode(treeObj, newCurrentTree, currentPath);
    setTreeObj(newTree);

    try {
      if (list?.[0]) {
        await db.tree.update(list?.[0].id, newTree as any);
      } else {
        await db.tree.add(newTree)
      }

      setStatus(`Item successfully added.`);
    } catch (error) {
      setStatus(`failed to add, ${error}`);
    }
  }

  if (!isLoaded) {
    return (
      <div>
        Loading...
      </div>
    );
  }

  if (!currentNode) {
    return (
      <div>
        Page Not found
      </div>
    );
  }

  return (
    <Context.Provider
      value={{
        status,
        updateTree,
        currentNode,
        currentPath,
        changePath,
      }}
    >
      {children}
    </Context.Provider>
  );
};

const useRouteContext = () => useContext(Context);

export {
  useRouteContext,
  SystemRoute,
};
